//
//  ColorPicker
//
//  Created by Orangebananaspy on 31.07.2015.
//  Copyright (c) 2015 Orangebananaspy. All rights reserved.
//


@protocol ColorPickerDelegate <NSObject>
@required
- (void)pickedColor:(UIColor *)color;
- (void)pickerReturnedError:(NSString *)error;
@end

@interface ColorPicker : UIViewController <UIAlertViewDelegate>
{
    id delegate;
}
@property (nonatomic, assign) id <ColorPickerDelegate> delegate;
- (void)portrait;
- (void)changeBackgroundColorTo:(UIColor *)color;
- (void)changeBackgroundImageTo:(UIImage *)image;
- (void)registerProgramName:(NSString *)stringProgram andDeveloperName:(NSString *)stringDeveloper;
- (_Bool)isRegistered;
@end
Color Picker

How to use it in your Theos Project:

1) Link ColorPicker Library to your project
    - in order to do this you will need to copy the "/usr/lib/ColorPicker.dylib" and paste it in /theos/lib/ with the name of "libColorPicker.dylib", the file path of the new file will look like "/theos/lib/libColorPicker.dylib".
    - in your make file paste this line "YOURTWEAKNAME_LDFLAGS = -lz -lColorPicker", replace "YOURTWEAKNAME" with your tweak name.

2) Include the header in your project
    - I mean its easy enough to copy the ColorPicker.h found in this git to your project

** The library returns a UIViewController the example simply shows you how to use it in Preference Bundle but you can get creative with this
//
//  Created by Orangebananaspy on 30.07.2015.
//  Copyright (c) 2015 Orangebananaspy. All rights reserved.
//

#import "PrefBundleExample.h"
#include <dlfcn.h>
#define tweakPlistPath @"/var/mobile/Library/Preferences/org.thebigboss.example.plist"

@implementation PrefBundleExample
/** Fixing iOS 8's Preference Bundle to actually save and such starts here **/
- (id)readPreferenceValue:(PSSpecifier*)specifier
{
    NSDictionary *exampleTweakSettings = [NSDictionary dictionaryWithContentsOfFile:tweakPlistPath];
    
    if(!exampleTweakSettings[specifier.properties[@"key"]])
    {
        return specifier.properties[@"default"];
    }
    
    return exampleTweakSettings[specifier.properties[@"key"]];
}

- (void)setPreferenceValue:(id)value specifier:(PSSpecifier*)specifier
{
    NSMutableDictionary *defaults = [NSMutableDictionary dictionary];
    [defaults addEntriesFromDictionary:[NSDictionary dictionaryWithContentsOfFile:tweakPlistPath]];
    [defaults setObject:value forKey:specifier.properties[@"key"]];
    [defaults writeToFile:tweakPlistPath atomically:YES];
    
    CFStringRef toPost = (__bridge CFStringRef)specifier.properties[@"PostNotification"];
    
    if(toPost)
    {
        CFNotificationCenterPostNotification(CFNotificationCenterGetDarwinNotifyCenter(), toPost, nil, nil, true);
    }
}
/** and ends here **/

/** Just loading the specifiers from plist **/
- (id)specifiers
{
	if(_specifiers == nil)
    {
		_specifiers = [self loadSpecifiersFromPlistName:@"PrefBundleExample" target:self];
    }
    
	return _specifiers;
}

/** Do the magical part **/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /** Initialize color picker only if color picker cell was clicked **/
    if([[tableView cellForRowAtIndexPath:indexPath] isKindOfClass:[PrefBundleExampleCell class]])
    {
        /** This somewhat fixes the issue when you double tap or tap too quickly it crashes due to the same viewcontroller not allowed to be pushed more than once **/
        if(![self.navigationController.topViewController isKindOfClass:[ColorPicker class]])
        {
            /** This is an extra step to make sure ColorPicker library is not broken before we run it **/
            void *lib_handle = dlopen("/usr/lib/ColorPicker.dylib", RTLD_LOCAL|RTLD_LAZY);
            
            if(lib_handle)
            {
                self.picker = [[ColorPicker alloc] init];//Initialize
                self.picker.delegate = self;//This is a must to get the colors from picker
                [self.picker registerProgramName:@"org.thebigboss.nudekeys" andDeveloperName:@"Orangebananaspy"];//register (this is purely for demographics and stats), its fine if you wish not to
                
                if([self.picker isRegistered])//make sure it has registered before you run the register restricted items (well not soooo much restricted as you are registered even if you really don't register but again register to show support to this library) :)
                {
                    [self.picker portrait];//change orientation to portrait
                    [self.picker changeBackgroundColorTo:[UIColor colorWithRed:0.941 green:0.937 blue:0.961 alpha:1]];//change background color (if left blank the default is set to white color)
                }
                
                self.indexPathCurrent = indexPath;//get clicked cells index
                
                double delayInSeconds = 0.1;//if you truly wish to change the orientaion its best if you delay before show the controller
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [self.navigationController pushViewController:self.picker animated:YES];//show the controller
                });
            }
            else
            {
                NSLog(@"%s", dlerror());//if there was an error opening the library
                exit(EXIT_FAILURE);//use this only if you want to make it obvious that the color picker library isnt working
            }
        }
    }
    else
    {
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];//do the normal stuff because the color picker wasnt selected
    }
}

/** A function which is required to have or will crash :P **/
- (void)pickerReturnedError:(NSString *)error//only called when there is an error
{
    /** Not a good idea to call UIAlertView to actually show an error, why not NSLog it and just keep it for developers incase if they need to debug ._. **/
    UIAlertView *saved = [[UIAlertView alloc] initWithTitle:@"Color Picker" message:error delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    [saved show];
}

/** A fucntion also required or will crash :p **/
- (void)pickedColor:(UIColor *)color//called whenever a color is selected by the user
{
    [self.navigationController popToRootViewControllerAnimated:YES];//get back to the main controller
    
     PrefBundleExampleCell *tmpCell = (PrefBundleExampleCell *)[self->_table cellForRowAtIndexPath:self.indexPathCurrent];//get a reference to the selected color picker cell
    [tmpCell saveCellWithColor:[UIColor hexValuesFromUIColor:color]];//perhaps save the cell colour!
    
    /** Maybe you want to let the user know that the colors been saved? not needed because they can see the cell changing colour **/
    UIAlertView *saved = [[UIAlertView alloc] initWithTitle:@"Color Picker" message:[color description] delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    [saved show];
}
@end

@implementation PrefBundleExampleCell
/** get an UIImage from a UIColor **/
- (UIImage *)imageFromColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

/** Save the cell color! **/
- (_Bool)saveCellWithColor:(NSString *)color
{
    NSMutableDictionary *defaults = [[NSMutableDictionary alloc] init];
    [defaults addEntriesFromDictionary:[NSDictionary dictionaryWithContentsOfFile:tweakPlistPath]];
    [defaults setObject:color forKey:keyValue];
    
    UIColor *selectedColorFromString = [UIColor colorWithHexString:color];//get the changed color
    self.selectedColor.image = [self imageFromColor:(selectedColorFromString ? selectedColorFromString : [UIColor clearColor])];//change the cell's image color to the selected color
    
    self.detailTextLabel.text = color;//give the user a hex value of what they selected

    return [defaults writeToFile:tweakPlistPath atomically:YES];//write it to the file
}

/** All this stuff is pretty straight forward if not well contact me any time :) **/
- (instancetype)initWithStyle:(int)style reuseIdentifier:(NSString *)reuseIdentifier specifier:(PSSpecifier *)specifier
{
    self = [super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseIdentifier specifier:specifier];
    
    if(self)
    {
        self.clipsToBounds = NO;
        self.layer.masksToBounds = NO;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        keyValue = [specifier.properties[@"key"] copy];
        NSString *keyLabel = [specifier.properties[@"keyLabel"] copy];
        
        NSDictionary *tweakSettings = [NSDictionary dictionaryWithContentsOfFile:tweakPlistPath];
        UIColor *selectedColorFromString = [UIColor colorWithHexString:tweakSettings[keyValue]];
        
        self.selectedColorView = [[UIView alloc] initWithFrame:CGRectMake(20, (self.frame.size.height / 2) + 2.5, self.frame.size.height - 5, self.frame.size.height - 5)];
        self.selectedColorView.layer.shadowColor = [UIColor blackColor].CGColor;
        self.selectedColorView.layer.shadowOffset = CGSizeMake(0, 0);
        self.selectedColorView.layer.shadowOpacity = 0.4;
        self.selectedColorView.layer.shadowRadius = 2.0;
        [self.contentView addSubview:self.selectedColorView];
        
        self.selectedColor = [[UIImageView alloc] initWithFrame:self.selectedColorView.bounds];
        self.selectedColor.image = [self imageFromColor:(selectedColorFromString ? selectedColorFromString : [UIColor clearColor])];
        self.selectedColor.layer.masksToBounds = YES;
        self.selectedColor.layer.cornerRadius = self.selectedColor.frame.size.width / 2.0;
        [self.selectedColorView addSubview:self.selectedColor];
        
        self.selectedColorLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.selectedColorView.frame.origin.x + self.selectedColorView.frame.size.width) + 20, self.selectedColorView.frame.origin.y, self.frame.size.width - (self.selectedColorView.frame.origin.x + self.selectedColorView.frame.size.width + 20), self.frame.size.height - 5)];
        self.selectedColorLabel.text = (keyLabel ? keyLabel : @"");
        self.selectedColorLabel.adjustsFontSizeToFitWidth = YES;
        self.selectedColorLabel.clipsToBounds = YES;
        self.selectedColorLabel.backgroundColor = [UIColor clearColor];
        self.selectedColorLabel.textColor = self.textLabel.textColor;
        self.selectedColorLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.selectedColorLabel];
        
        self.detailTextLabel.text = tweakSettings[keyValue];
    }
    
    return self;
}
@end
//
//  Created by Orangebananaspy on 30.07.2015.
//  Copyright (c) 2015 Orangebananaspy. All rights reserved.
//

#import "Preferences/PSListController.h"
#import "Preferences/PSSpecifier.h"
#import <PSTableCell.h>
#import "UIColor+HexColors.h"
#import "../../ColorPicker.h"

@interface PrefBundleExample : PSListController <ColorPickerDelegate>
@property (nonatomic, strong) ColorPicker *picker;
@property (nonatomic, strong) NSIndexPath *indexPathCurrent;
@end

@interface PrefBundleExampleCell : PSTableCell
{
    NSString *keyValue;
}
@property (nonatomic, strong) UIView *selectedColorView;
@property (nonatomic, strong) UIImageView *selectedColor;
@property (nonatomic, strong) UILabel *selectedColorLabel;
- (_Bool)saveCellWithColor:(NSString *)color;
@end